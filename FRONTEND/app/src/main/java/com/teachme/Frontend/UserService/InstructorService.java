package com.teachme.Frontend.UserService;

import com.teachme.Frontend.Models.Course;
import com.teachme.Frontend.Models.Instructor;

import java.util.List;
import java.util.Set;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface InstructorService {

    @GET("/instructor/getAll")
    public Call<List<Instructor>> getAllUsers();

    @GET("/instructor/login/{username}/{password}")
    public Call<Instructor> getUserByUsernameAndPassword(@Path("username") String username, @Path("password") String password);

    @GET("/instructor/{instructorId}/courses")
    public Call<Set<Course>> getInstructorCourses(@Path("instructorId") Long id);

}