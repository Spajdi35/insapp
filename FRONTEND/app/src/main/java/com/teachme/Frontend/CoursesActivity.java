package com.teachme.Frontend;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.teachme.Frontend.Models.Course;
import com.teachme.Frontend.Models.Instructor;
import com.teachme.R;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class CoursesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_courses);

        Instructor instructor = (Instructor)getIntent().getSerializableExtra("currentUser");

        //
        //
        //
        // OBAVEZNO PREUREDIT NA ISPRAVAN NACIN

        ArrayList<Course> courses = new ArrayList<>();
        courses.addAll(instructor.getCourses());

        System.out.println("KURSEVI: " + courses);

        final ListView listView = (ListView)findViewById(R.id.list);
        final InstructorAdapter instructorAdapter = new InstructorAdapter(this, courses);
        new Thread(new Runnable() {
            @Override
            public void run() {
                listView.setAdapter(instructorAdapter);
            }
        }).run();

        // DO OVDJE
        //
        //
        //
    }
}
