package com.teachme.Frontend.Models;

import java.io.Serializable;

public class CourseDetail implements Serializable {

    private Long id;

    private String description;

    private Course course;

    public CourseDetail() {

    }

    public CourseDetail(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

}
