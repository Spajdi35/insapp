package com.teachme.Frontend;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.teachme.Frontend.Models.Instructor;
import com.teachme.Frontend.UserService.InstructorService;
import com.teachme.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private EditText username;
    private EditText password;
    private Button btnSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        username = (EditText) findViewById(R.id.input_username);
        password = (EditText) findViewById(R.id.input_password);
        btnSignIn = (Button) findViewById(R.id.btn_signin);

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InstructorService userService = HostServiceGenerator.createService(InstructorService.class);
                Call<Instructor> callAsync = userService.getUserByUsernameAndPassword(username.getText().toString(), password.getText().toString());
                System.out.println("IMEUSERA: " + username.getText().toString() + " " + password.getText().toString());

                callAsync.enqueue(new Callback<Instructor>() {
                    @Override
                    public void onResponse(Call<Instructor> call, final Response<Instructor> response) {
                        System.out.println("KORISNIK: " + response.body());
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Intent i = new Intent(MainActivity.this, CoursesActivity.class);
                                i.putExtra("currentUser", response.body());
                                MainActivity.this.startActivity(i);
                            }
                        }).run();
                    }

                    @Override
                    public void onFailure(Call<Instructor> call, Throwable t) {
                        //  NEKI POPUP KOJI KAZE ILI DA USER NE POSTOJI ILI JE KRIV USERNAME ILI PASSWORD TO JA MORAM U BAZI JOS
                    }
                });

                //  PRELAZAK U NOVI SCREEN - LISTVIEW

            }
        });

    }
}
