package com.teachme.Frontend;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.teachme.Frontend.Models.Course;
import com.teachme.Frontend.Models.Instructor;
import com.teachme.R;

import java.util.ArrayList;

public class InstructorAdapter extends ArrayAdapter<Course> {

    public InstructorAdapter(Activity context, ArrayList<Course> courses) {
        super(context, 0, courses);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItemView = convertView;

        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.list_row, parent, false);
        }

        final Course course = getItem(position);
        final TextView title = (TextView)listItemView.findViewById(R.id.title);
        final TextView description = (TextView)listItemView.findViewById(R.id.description);

        new Thread(new Runnable() {
            @Override
            public void run() {

                if (course != null) {
                    title.setText(course.getCourseName());
                    if (course.getCourseDetail() != null) {
                        description.setText(course.getCourseDetail().getDescription());
                    }
                }

            }
        }).run();

        return listItemView;
    }
}
