package com.teachme.TeachMe.dao;

import com.teachme.TeachMe.domain.Instructor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface InstructorRepository extends JpaRepository<Instructor, Long> {
    int countByEmail(String email);
    Optional<Instructor> findByUsernameAndPassword(String username, String password);
    Optional<Instructor> findByUsername(String username);
    Optional<Instructor> findById(Long id);
}
