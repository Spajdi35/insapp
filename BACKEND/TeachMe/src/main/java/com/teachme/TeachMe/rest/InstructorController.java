package com.teachme.TeachMe.rest;

import com.teachme.TeachMe.domain.Course;
import com.teachme.TeachMe.domain.Instructor;
import com.teachme.TeachMe.service.CourseService;
import com.teachme.TeachMe.service.InstructorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/instructor")
public class InstructorController {

    @Autowired
    private InstructorService instructorService;

    @GetMapping("/getAll")
    public List<Instructor> listAll() {
        return instructorService.listAll();
    }

    @PostMapping("/create")
    public Instructor createUser(@RequestBody Instructor user) {
        return instructorService.createInstructor(user);
    }

    @GetMapping("/login/{username}/{password}")
    public Instructor login(@PathVariable("username") String username, @PathVariable("password") String password) {
        return instructorService.login(username, password);
    }

    @GetMapping("/get/{id}")
    public Instructor get(@PathVariable("id") Long id) {
        return instructorService.get(id);
    }

    @PostMapping("/{id}/addCourse")
    public Course addCourse(@PathVariable("id") Long instructorId,@RequestBody Course course) {
        return instructorService.addCourse(instructorId, course);
    }

    @GetMapping("/instructor/{instructorId}/courses")
    public Set<Course> getInstructorCourses(@PathVariable("instructorId") Long id) {
        return instructorService.getCourses(id);
    }

}
