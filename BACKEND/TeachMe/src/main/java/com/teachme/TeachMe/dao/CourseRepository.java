package com.teachme.TeachMe.dao;

import com.teachme.TeachMe.domain.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course, Long> {
}
