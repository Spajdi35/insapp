package com.teachme.TeachMe.rest;

import com.teachme.TeachMe.domain.Course;
import com.teachme.TeachMe.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/course")
public class CourseController {

    @Autowired
    private CourseService courseService;

    @GetMapping("/get/{id}")
    public Course get(@PathVariable("id") Long id) {
        return courseService.get(id);
    }

}
