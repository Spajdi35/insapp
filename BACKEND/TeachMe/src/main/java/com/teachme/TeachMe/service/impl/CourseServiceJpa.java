package com.teachme.TeachMe.service.impl;

import com.teachme.TeachMe.dao.CourseRepository;
import com.teachme.TeachMe.domain.Course;
import com.teachme.TeachMe.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceJpa implements CourseService {

    @Autowired
    private CourseRepository courseRepository;

    @Override
    public Course createCourse(Course course) {
        return courseRepository.save(course);
    }

    @Override
    public Course get(Long id) {
        return courseRepository.findById(id).orElseThrow(
                () -> new RuntimeException("No course with id " + id)
        );
    }
}
