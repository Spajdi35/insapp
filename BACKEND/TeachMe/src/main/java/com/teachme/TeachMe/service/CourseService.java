package com.teachme.TeachMe.service;

import com.teachme.TeachMe.domain.Course;

public interface CourseService {
    public Course createCourse(Course course);
    public Course get(Long id);
}
