package com.teachme.TeachMe.service;

import com.teachme.TeachMe.domain.Course;
import com.teachme.TeachMe.domain.Instructor;

import java.util.List;
import java.util.Set;

public interface InstructorService {
    public Instructor createInstructor(Instructor user);
    public List<Instructor> listAll();
    public Instructor login(String username, String password);
    public Instructor get(Long id);
    public Course addCourse(Long instructorId, Course course);
    public Set<Course> getCourses(Long instructorId);
}
