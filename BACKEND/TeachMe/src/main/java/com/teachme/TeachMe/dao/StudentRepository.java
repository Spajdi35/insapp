package com.teachme.TeachMe.dao;

import com.teachme.TeachMe.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
