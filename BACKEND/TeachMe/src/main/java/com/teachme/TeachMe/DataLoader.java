package com.teachme.TeachMe;

import com.teachme.TeachMe.dao.CourseRepository;
import com.teachme.TeachMe.dao.InstructorRepository;
import com.teachme.TeachMe.domain.Course;
import com.teachme.TeachMe.domain.CourseComment;
import com.teachme.TeachMe.domain.CourseDetail;
import com.teachme.TeachMe.domain.Instructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class DataLoader implements ApplicationRunner {

    private InstructorRepository instructorRepository;
    private CourseRepository courseRepository;

    @Autowired
    public DataLoader(InstructorRepository userRepository, CourseRepository courseRepository) {
        this.instructorRepository = userRepository;
        this.courseRepository = courseRepository;
    }

    public void run(ApplicationArguments args) {
        Instructor instructor1 = new Instructor("mandingo", "123", "Mandingo", "Veliki", "mandingo@gmail.com");
        Instructor instructor2 = new Instructor("zvonkec", "123", "Pickica", "Mala", "zvone@gmail.com");
//        Instructor instructor3 = new Instructor("abc", "abc", "abc", "abc", "abc@gmail.com");
//        instructorRepository.save(instructor2);
//        instructorRepository.save(instructor3);
        Course course = new Course("Matematika");
        Course course1 = new Course("Hrvatski");
        Course course2 = new Course("Biologija");
        course2.add(new CourseComment("Vrlo dobar kurs"));
        course2.add(new CourseComment("Instruktor iznimno dobro objasnjava"));
        course.setCourseDetail(new CourseDetail("Dost dobar kurs"));
        course1.setCourseDetail(new CourseDetail("Hrvatski je dosadan"));
        course2.setCourseDetail(new CourseDetail("Nepotrebno"));
        instructor1.add(course);
        instructor1.add(course1);
        instructor1.add(course2);
        instructor2.add(new Course("Matematika"));
        instructorRepository.save(instructor1);
        instructorRepository.save(instructor2);
        System.out.println("Kursevi instruktora: " + instructor1.getCourses());
        System.out.println("Opis kursa instruktora(MATEMATIKA): " + instructor1.getCourseByName("Matematika").getCourseDetail());
        System.out.println("Opis kursa instruktora(HRVATSKI): " + instructor1.getCourseByName("Hrvatski").getCourseDetail());
        System.out.println("Opis kursa instruktora(BIOLOGIJA): " + instructor1.getCourseByName("Biologija").getCourseDetail());
    }
}
