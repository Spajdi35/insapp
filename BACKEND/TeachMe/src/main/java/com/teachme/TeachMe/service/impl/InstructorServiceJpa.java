package com.teachme.TeachMe.service.impl;

import com.teachme.TeachMe.dao.InstructorRepository;
import com.teachme.TeachMe.domain.Course;
import com.teachme.TeachMe.domain.Instructor;
import com.teachme.TeachMe.service.Exceptions.UserAlreadyExistsException;
import com.teachme.TeachMe.service.Exceptions.UserNotExistException;
import com.teachme.TeachMe.service.InstructorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class InstructorServiceJpa implements InstructorService {

    @Autowired
    private InstructorRepository instructorRepository;

    @Override
    public Instructor createInstructor(Instructor instructor) {

        if (instructorRepository.countByEmail(instructor.getEmail()) > 0) {
            throw new UserAlreadyExistsException("User with email: " + instructor.getEmail() + " already exists");
        }

        return instructorRepository.save(instructor);
    }

    @Override
    public List<Instructor> listAll() {
        return instructorRepository.findAll();
    }

    @Override
    public Instructor login(String username, String password) {

        if (!instructorRepository.findByUsername(username).isPresent()) {
            throw new UserNotExistException("User with username " + username + " does not exists");
        }

        return instructorRepository.findByUsernameAndPassword(username, password).orElseThrow(
                () -> new UserNotExistException("Wrong username or password")
        );
    }

    @Override
    public Instructor get(Long instructorId) {
        return instructorRepository.findById(instructorId).orElseThrow(
                () -> new UserNotExistException("No user with id " + instructorId)
        );
    }

    @Override
    public Course addCourse(Long instructorId, Course course) {
        Instructor instructor = get(instructorId);
        instructor.add(course);

        instructorRepository.save(instructor);
        return course;
    }

    @Override
    public Set<Course> getCourses(Long instructorId) {
        Instructor instructor = get(instructorId);

        return instructor.getCourses();
    }
}
